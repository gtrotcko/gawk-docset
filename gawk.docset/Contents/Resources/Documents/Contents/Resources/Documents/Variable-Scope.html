<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><!-- 
Copyright (C) 1989, 1991, 1992, 1993, 1996-2005, 2007, 2009-2015 

Free Software Foundation, Inc.



This is Edition 4.1 of GAWK: Effective AWK Programming: A User's Guide for GNU Awk,
for the 4.1.3 (or later) version of the GNU
implementation of AWK.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with the
Invariant Sections being "GNU General Public License", with the
Front-Cover Texts being "A GNU Manual", and with the Back-Cover Texts
as in (a) below.
A copy of the license is included in the section entitled
"GNU Free Documentation License".

a. The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual." --><!-- Created by GNU Texinfo 6.1, http://www.gnu.org/software/texinfo/ --><head>
<a class="dashingAutolink" name="autolink-402"></a><a class="dashAnchor" name="//apple_ref/cpp/Package/The%20GNU%20Awk%20User%E2%80%99s%20Guide%3A%20Variable%20Scope"></a><title>The GNU Awk User&rsquo;s Guide: Variable Scope</title>

<meta name="description" content="The GNU Awk User&rsquo;s Guide: Variable Scope"/>
<meta name="keywords" content="The GNU Awk User&rsquo;s Guide: Variable Scope"/>
<meta name="resource-type" content="document"/>
<meta name="distribution" content="global"/>
<meta name="Generator" content="makeinfo"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="index.html#Top" rel="start" title="Top"/>
<link href="Index.html#Index" rel="index" title="Index"/>
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents"/>
<link href="Function-Caveats.html#Function-Caveats" rel="up" title="Function Caveats"/>
<link href="Pass-By-Value_002fReference.html#Pass-By-Value_002fReference" rel="next" title="Pass By Value/Reference"/>
<link href="Calling-A-Function.html#Calling-A-Function" rel="prev" title="Calling A Function"/>
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<a name="Variable-Scope"></a>
<div class="header">
<p>
Next: <a href="Pass-By-Value_002fReference.html#Pass-By-Value_002fReference" accesskey="n" rel="next">Pass By Value/Reference</a>, Previous: <a href="Calling-A-Function.html#Calling-A-Function" accesskey="p" rel="prev">Calling A Function</a>, Up: <a href="Function-Caveats.html#Function-Caveats" accesskey="u" rel="up">Function Caveats</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr/>
<a name="Controlling-Variable-Scope"></a>
<h4 class="subsubsection">9.2.3.2 Controlling Variable Scope</h4>

<a name="index-local-variables_002c-in-a-function"></a>
<a name="index-variables_002c-local-to-a-function"></a>
<p>Unlike in many languages,
there is no way to make a variable local to a <code>{</code> &hellip; <code>}</code> block in
<code>awk</code>, but you can make a variable local to a function. It is
good practice to do so whenever a variable is needed only in that
function.
</p>
<p>To make a variable local to a function, simply declare the variable as
an argument after the actual function arguments
(see <a href="Definition-Syntax.html#Definition-Syntax">Definition Syntax</a>).
Look at the following example, where variable
<code>i</code> is a global variable used by both functions <code>foo()</code> and
<code>bar()</code>:
</p>
<div class="example">
<pre class="example">function bar()
{
    for (i = 0; i &lt; 3; i++)
        print &#34;bar&#39;s i=&#34; i
}

function foo(j)
{
    i = j + 1
    print &#34;foo&#39;s i=&#34; i
    bar()
    print &#34;foo&#39;s i=&#34; i
}

BEGIN {
      i = 10
      print &#34;top&#39;s i=&#34; i
      foo(0)
      print &#34;top&#39;s i=&#34; i
}
</pre></div>

<p>Running this script produces the following, because the <code>i</code> in
functions <code>foo()</code> and <code>bar()</code> and at the top level refer to the same
variable instance:
</p>
<div class="example">
<pre class="example">top&#39;s i=10
foo&#39;s i=1
bar&#39;s i=0
bar&#39;s i=1
bar&#39;s i=2
foo&#39;s i=3
top&#39;s i=3
</pre></div>

<p>If you want <code>i</code> to be local to both <code>foo()</code> and <code>bar()</code>, do as
follows (the extra space before <code>i</code> is a coding convention to
indicate that <code>i</code> is a local variable, not an argument):
</p>
<div class="example">
<pre class="example">function bar(    i)
{
    for (i = 0; i &lt; 3; i++)
        print &#34;bar&#39;s i=&#34; i
}

function foo(j,    i)
{
    i = j + 1
    print &#34;foo&#39;s i=&#34; i
    bar()
    print &#34;foo&#39;s i=&#34; i
}

BEGIN {
      i = 10
      print &#34;top&#39;s i=&#34; i
      foo(0)
      print &#34;top&#39;s i=&#34; i
}
</pre></div>

<p>Running the corrected script produces the following:
</p>
<div class="example">
<pre class="example">top&#39;s i=10
foo&#39;s i=1
bar&#39;s i=0
bar&#39;s i=1
bar&#39;s i=2
foo&#39;s i=1
top&#39;s i=10
</pre></div>

<p>Besides scalar values (strings and numbers), you may also have
local arrays.  By using a parameter name as an array, <code>awk</code>
treats it as an array, and it is local to the function.
In addition, recursive calls create new arrays.
Consider this example:
</p>
<div class="example">
<pre class="example">function some_func(p1,      a)
{
    if (p1++ &gt; 3)
        return

    a[p1] = p1

    some_func(p1)

    printf(&#34;At level %d, index %d %s found in a\n&#34;,
         p1, (p1 - 1), (p1 - 1) in a ? &#34;is&#34; : &#34;is not&#34;)
    printf(&#34;At level %d, index %d %s found in a\n&#34;,
         p1, p1, p1 in a ? &#34;is&#34; : &#34;is not&#34;)
    print &#34;&#34;
}

BEGIN {
    some_func(1)
}
</pre></div>

<p>When run, this program produces the following output:
</p>
<div class="example">
<pre class="example">At level 4, index 3 is not found in a
At level 4, index 4 is found in a

At level 3, index 2 is not found in a
At level 3, index 3 is found in a

At level 2, index 1 is not found in a
At level 2, index 2 is found in a
</pre></div>

<hr/>
<div class="header">
<p>
Next: <a href="Pass-By-Value_002fReference.html#Pass-By-Value_002fReference" accesskey="n" rel="next">Pass By Value/Reference</a>, Previous: <a href="Calling-A-Function.html#Calling-A-Function" accesskey="p" rel="prev">Calling A Function</a>, Up: <a href="Function-Caveats.html#Function-Caveats" accesskey="u" rel="up">Function Caveats</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>





</body></html>